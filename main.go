package main

import (
	"github.com/go-playground/validator/v10"
	_ "github.com/go-sql-driver/mysql"
	"github.com/julienschmidt/httprouter"
	"go-template/app"
	"go-template/controller"
	"go-template/exception"
	"go-template/helper"
	"go-template/middleware"
	"go-template/repository"
	"go-template/service"
	"net/http"
)

func main() {
	newDB:= app.NewDB()
	validate := validator.New()

	router:= httprouter.New()

	categoryRepository := repository.NewCategoryRepository()
	categoryService := service.NewCategoryService(categoryRepository,newDB,validate)
	categoryController := controller.NewCategoryController(categoryService)

	router.GET("/api/categories",categoryController.FindAll)
	router.GET("/api/categories/:categoryId",categoryController.FIndById)
	router.POST("/api/categories",categoryController.Create)
	router.PUT("/api/categories/:categoryId",categoryController.Update)
	router.DELETE("/api/categories/:categoryId",categoryController.Delete)

	router.PanicHandler = exception.ErrorHandler
	server := http.Server{
		Addr: ":3000",
		Handler: middleware.NewAuthMiddleware(router),
	}

	err:=server.ListenAndServe()
	helper.PanicIfError(err)
}