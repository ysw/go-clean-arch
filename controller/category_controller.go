package controller

import (
	"net/http"
	"github.com/julienschmidt/httprouter"
)

//github.com/julienschmidt/httprouter
type CategoryController interface {
	Create(w http.ResponseWriter,request *http.Request,params httprouter.Params)
	Update(w http.ResponseWriter,request *http.Request,params httprouter.Params)
	Delete(w http.ResponseWriter,request *http.Request,params httprouter.Params)
	FIndById(w http.ResponseWriter,request *http.Request,params httprouter.Params)
	FindAll(w http.ResponseWriter,request *http.Request,params httprouter.Params)
}