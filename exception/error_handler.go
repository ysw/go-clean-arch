package exception

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"go-template/helper"
	"go-template/model/web"
	"net/http"
)

func ErrorHandler(w http.ResponseWriter,request *http.Request,err interface{}){

	if notFoundError(w,request,err){
		return 
	}

	if validationError(w,request,err){
		return
	}

	internalServerError(w,request,err)

}

func validationError(w http.ResponseWriter, request *http.Request, err interface{}) bool {
	exception, ok := err.(validator.ValidationErrors)
	if ok{
		w.Header().Set("Content-Type","applicaton/json")
		w.WriteHeader(http.StatusBadRequest)
		webResponse := web.WebResponse{
			Code: http.StatusBadRequest,
			Status: "BAD REQUEST",
			Data: exception.Error(),
		}
		helper.WriteToResponseBody(w,webResponse)
		return true
	}else {
		return false
	}
}

func notFoundError(w http.ResponseWriter, request *http.Request, err interface{}) bool {
	//fmt.Println(err)
	exception,ok := err.(NotFoundError)
	if ok {
		w.Header().Set("Content-Type","applicaton/json")
		w.WriteHeader(http.StatusNotFound)
		webResponse := web.WebResponse{
			Code: http.StatusNotFound,
			Status: "NOT FOUND",
			Data: exception.Error,
		}
		helper.WriteToResponseBody(w,webResponse)
		return true
	}else {
		return false
	}

}

func internalServerError(w http.ResponseWriter, request *http.Request, err interface{}) {
	fmt.Println(err)
	w.Header().Set("Content-Type","applicaton/json")
	w.WriteHeader(http.StatusInternalServerError)
	webResponse := web.WebResponse{
		Code: http.StatusInternalServerError,
		Status: "INTERNAL SERVER ERROR",
		//Data: e.(string),
	}
	helper.WriteToResponseBody(w,webResponse)
}
