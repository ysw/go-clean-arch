package controller

import (
	"github.com/julienschmidt/httprouter"
	"go-template/helper"
	"go-template/model/web"
	"go-template/service"
	"net/http"
	"strconv"
)

type CategoryControllerImpl struct {
	CategoryService service.CategoryService
}

func NewCategoryController(categoryService service.CategoryService) CategoryController {
	return &CategoryControllerImpl{
		CategoryService: categoryService,
	}
}

func (c CategoryControllerImpl) Create(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	//decoder:=json.NewDecoder(request.Body)
	//categoryCreateRequest := web.CategoryCreateRequest{}
	//err := decoder.Decode(&categoryCreateRequest)
	//helper.PanicIfError(err)
	categoryCreateRequest := web.CategoryCreateRequest{}
	helper.ReadFromRequestBody(request,&categoryCreateRequest)

	categoryResponse:= c.CategoryService.Create(request.Context(),categoryCreateRequest)
	webResponse:= web.WebResponse{
		Code: 200,
		Status: "OK",
		Data: categoryResponse,
	}

	//w.Header().Add("Content-Type","application/json")
	//encoder:= json.NewEncoder(w)
	//err = encoder.Encode(webResponse)

	helper.WriteToResponseBody(w,webResponse)
}

func (c CategoryControllerImpl) Update(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	//decoder:=json.NewDecoder(request.Body)
	//categoryUpdateRequest := web.CategoryUpateRequest{}
	//err := decoder.Decode(&categoryUpdateRequest)
	//helper.PanicIfError(err)

	categoryUpdateRequest := web.CategoryUpdateRequest{}
	helper.ReadFromRequestBody(request,&categoryUpdateRequest)

	categoryId := params.ByName("categoryId")
	id, err := strconv.Atoi(categoryId)
	helper.PanicIfError(err)

	categoryUpdateRequest.Id=id

	categoryResponse:= c.CategoryService.Update(request.Context(),categoryUpdateRequest)
	webResponse:= web.WebResponse{
		Code: 200,
		Status: "OK",
		Data: categoryResponse,
	}

	helper.WriteToResponseBody(w,webResponse)
}

func (c CategoryControllerImpl) Delete(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	categoryId := params.ByName("categoryId")
	id, err := strconv.Atoi(categoryId)
	helper.PanicIfError(err)

    c.CategoryService.Delete(request.Context(),id)
	webResponse:= web.WebResponse{
		Code: 200,
		Status: "OK",
	}

	helper.WriteToResponseBody(w,webResponse)
}

func (c CategoryControllerImpl) FIndById(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	categoryId := params.ByName("categoryId")
	id, err := strconv.Atoi(categoryId)
	helper.PanicIfError(err)

	categoryResponse:= c.CategoryService.FindById(request.Context(),id)
	webResponse:= web.WebResponse{
		Code: 200,
		Status: "OK",
		Data: categoryResponse,
	}

	helper.WriteToResponseBody(w,webResponse)
}

func (c CategoryControllerImpl) FindAll(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	categoryResponses:= c.CategoryService.FindAll(request.Context())
	webResponse:= web.WebResponse{
		Code: 200,
		Status: "OK",
		Data: categoryResponses,
	}

	helper.WriteToResponseBody(w,webResponse)
}

